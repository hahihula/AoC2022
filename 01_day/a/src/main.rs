use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
  println!("Hello, world!");
  if let Ok(lines) = read_lines("../input.txt") {
    let elves = calories_per_elf(lines);
    println!("{:?}", elves.iter().max());
  } else {
    println!("Can not read the input file");
  }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
  let file = File::open(filename)?;
  Ok(io::BufReader::new(file).lines())
}

fn calories_per_elf(lines: io::Lines<io::BufReader<File>>) -> Vec<i32> {
  let mut elves = Vec::new();
  let mut current_elf = 0;
  for line in lines {
    if let Ok(string_number) = line {
      match string_number.is_empty() {
        true => {
          elves.push(current_elf);
          current_elf = 0;
        },
        false => current_elf += string_number.parse::<i32>().unwrap(),
          
      }      
    }
  };
  return elves;
}

#[cfg(test)]
mod tests {
  #[test]
  fn it_works() {
    let result = 2 + 2;
    assert_eq!(result, 4);
  }

  #[test]
  fn parse_string() {
    let four = "4".parse::<u32>();
    assert_eq!(Ok(4), four);
  }

  #[test]
  fn parse_empty_string() {
    let zero = "".parse::<u32>();
    assert_eq!(Ok(0), zero);
  }
}
