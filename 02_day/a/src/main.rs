use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
  if let Ok(lines) = read_lines("../input.txt") {
    let score = strategy_to_score(lines);
    println!("{:?}", score);
  } else {
    println!("Can not read the input file");
  }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
  let file = File::open(filename)?;
  Ok(io::BufReader::new(file).lines())
}

fn strategy_to_score(lines: io::Lines<io::BufReader<File>>) -> i32 {
  let mut score = 0;
  for line in lines {
    if let Ok(round_log) = line {
      // let v: Vec<&str> = round_log.split(' ').collect();
      match round_log.as_str() {
        "A X" => score += 4,
        "A Y" => score += 8,
        "A Z" => score += 3,
        "B X" => score += 1,
        "B Y" => score += 5,
        "B Z" => score += 9,
        "C X" => score += 7,
        "C Y" => score += 2,
        "C Z" => score += 6,
        _ => score += 0,
      }      
    }
  };
  return score;
}

#[cfg(test)]
mod tests {
  #[test]
  fn it_works() {
    let result = 2 + 2;
    assert_eq!(result, 4);
  }

}
